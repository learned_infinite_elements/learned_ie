from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi
import numpy as np
from ngsolve import Mesh as NGSMesh
from compute_pot import local_polynomial_estimator,logder
np.random.seed(seed=1)
from ngsolve import *
from mpmath import mpc,whitw,diff
from mpmath import sqrt as mp_sqrt
import mpmath as mp
ngsglobals.msg_level = 0
SetNumThreads(4)

#################################################### 
# parameters
####################################################

order_ODE = 8 # order of ODE discretization
f_hz = 0.007 # frequency in mHz
a = 1.000699 # coupling radius for Atmo extension
spline_order = 6 # order of Spline approx. for coefficients
show_plots = True # show matplotlib plots
if show_plots:
    import matplotlib.pyplot as plt


#################################################### 
# some auxiliary functions
####################################################

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - mp_sqrt(z)
    else:
        return mp_sqrt(z)

def Make1DMesh_flex(R_min,R_max,N_elems=50):

    mesh_1D = netmesh(dim=1)
    pids = []   
    delta_r = (R_max-R_min)/N_elems
    for i in range(N_elems+1):
        pids.append (mesh_1D.Add (MeshPoint(Pnt(R_min + delta_r*i, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

#################################################### 
# damping model
####################################################

RSun = 6.963e8 # radius of the Sun in m 
omega_f = f_hz*2*pi*RSun
gamma0 = 2*pi*4.29*1e-6*RSun
omega0 = 0.003*2*pi*RSun
if f_hz < 0.0053:
    gamma = gamma0*(omega_f/omega0)**(5.77)
else:
    gamma = gamma0*(0.0053*2*pi*RSun/omega0)**(5.77)
omega_squared = omega_f**2+2*1j*omega_f*gamma

#################################################### 
# load solar models and compute potential
####################################################

rS,cS,rhoS,_,__,___ = np.loadtxt("modelSinput.txt",unpack=True)
rV,vV,rhoV,Tv  = np.loadtxt("VALCinput.txt",unpack=True)

# sound speed is not given in VAL-C model
# calculate it from temperature using the ideal gas law
gamma_ideal = 5/3 # adiabatic index
mu_ideal = 1.3*10**(-3) # mean molecular weight 
R_gas = 8.31446261815324 # universal gas constant 
cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)

# scale to SI units
cS = (10**-2)*cS
rhoS = (10**3)*rhoS
rhoV = (10**3)*rhoV

# overlap interval
rmin = rV[-1]
rmax = rS[0]

weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

r = np.concatenate((rS,rV)) 
ind = np.argsort(r)
r = r[ind]
c = np.concatenate((cS,cV))[ind]
rho = np.concatenate((rhoS,rhoV))[ind]
weight = np.concatenate((weightS,weightV))[ind]

rho_clean,drho,ddrho = logder(rho,r,weight,3)
c_clean = logder(c,r,weight,1)

#idx1 = np.min(np.where(r == a)[0])
idx1 = np.argmin(np.abs(r - a))
alpha_infty = (-drho/rho_clean)[idx1]
c_infty = c_clean[idx1]
print("alpha_infty = ",  alpha_infty) 
print("c_infty = ", c_infty)

i0 = np.min( np.nonzero(r > 0.99)[0] ) 

f,df,ddf = logder(1/np.sqrt(rho),r,weight,3)
pot_rho = np.sqrt(rho_clean)*(ddf+2*df/r)
pot_c = c_clean

c_atmo = c_clean.copy()
c_atmo[idx1:] = c_infty
rho_atmo = rho_clean.copy()
rho_atmo[idx1:] = rho_clean[idx1]*np.exp(-alpha_infty*(r[idx1:] - r[idx1]))

# exporting results for plotting:
# first sound speed and density for the 
# different models
plot_collect = [r[i0:]]
header = "r cV rhoV cA rhoA"
plot_collect = [r[i0:],c_clean[i0:],rho_clean[i0:],c_atmo[i0:],rho_atmo[i0:]]

np.savetxt(fname="c-rho-VALC.dat",
           X=np.transpose(plot_collect), 
           header = header,
           comments='' 
          )

if show_plots:
    #plt.plot(r[i0:],10**(-3)*c_clean[i0:],label='VAL-C')
    plt.semilogy(r[i0:],c_clean[i0:],label='VAL-C')
    plt.xlabel("r [solar radius]")
    plt.title("Sound speed")
    plt.legend()
    plt.show()

    plt.semilogy(r[i0:],rho_clean[i0:],label='VAL-C')
    plt.xlabel("r [solar radius]")
    plt.title("Density")
    plt.legend()
    plt.show()

# now the effective potential 
header_V = "r potVreal potVimag"
potV = pot_rho[i0:] - omega_squared/pot_c[i0:]**2
plot_collect_pV = [r[i0:],potV.real,potV.imag]

np.savetxt(fname="eff-pot-VALC.dat",
           X=np.transpose(plot_collect_pV), 
           header = header_V,
           comments='' 
          )

if show_plots:
    plt.plot(r[i0:],potV.real,label='VAL-C')
    plt.xlabel("r [solar radius]")
    plt.title("Real part of effective potential")
    plt.legend()
    plt.show()

    plt.plot(r[i0:],potV.imag,label='VAL-C')
    plt.xlabel("r [solar radius]")
    plt.legend()
    plt.show()

