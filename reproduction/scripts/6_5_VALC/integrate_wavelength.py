from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from ngsolve import Mesh as NGSMesh
from compute_pot import local_polynomial_estimator,logder
np.random.seed(seed=1)
from ngsolve import *
import mpmath as mp
ngsglobals.msg_level = 0
SetNumThreads(4)
from cmath import sqrt as csqrt

#################################################### 
# parameters
####################################################

order_ODE = 8 # order of ODE discretization
L_max = 6000 # how many DtN numbers to take
f_hz = 0.007 # frequency in mHz
a = 1.0 # coupling radius 
spline_order = 6 # order of Spline approx. for coefficients
R_min = a # radius of truncation boundary
R_max_ODE = 1.0033 # to how far out the ODE should be solved

#################################################### 
# some auxiliary functions
####################################################

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - csqrt(z)
    else:
        return csqrt(z)

def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])

def Make1DMesh_flex(R_min,R_max,N_elems=50):

    mesh_1D = netmesh(dim=1)
    pids = []
    coord = [] 
    delta_r = (R_max-R_min)/N_elems
    for i in range(N_elems+1):
        coord.append(R_min + delta_r*i)
        pids.append (mesh_1D.Add (MeshPoint(Pnt(R_min + delta_r*i, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

#################################################### 
# damping model
####################################################

RSun = 6.963e8 # radius of the Sun in m 
omega_f = f_hz*2*pi*RSun
gamma0 = 2*pi*4.29*1e-6*RSun
omega0 = 0.003*2*pi*RSun
if f_hz < 0.0053:
    gamma = gamma0*(omega_f/omega0)**(5.77)
else:
    gamma = gamma0*(0.0053*2*pi*RSun/omega0)**(5.77)
gamma = 0
omega_squared = omega_f**2+2*1j*omega_f*gamma
omega = sqrt_2branch(omega_squared).real

#################################################### 
# load solar models and compute potential
####################################################

rS,cS,rhoS,_,__,___ = np.loadtxt("modelSinput.txt",unpack=True)
rV,vV,rhoV,Tv  = np.loadtxt("VALCinput.txt",unpack=True)

# sound speed is not given in VAL-C model
# calculate it from temperature using the ideal gas law
gamma_ideal = 5/3 # adiabatic index
mu_ideal = 1.3*10**(-3) # mean molecular weight 
R_gas = 8.31446261815324 # universal gas constant 
cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)
# scale to SI units
cS = (10**-2)*cS
rhoS = (10**3)*rhoS
rhoV = (10**3)*rhoV

# overlap interval
rmin = rV[-1]
rmax = rS[0]

weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

r = np.concatenate((rS,rV)) 
ind = np.argsort(r)
r = r[ind]
c = np.concatenate((cS,cV))[ind]
rho = np.concatenate((rhoS,rhoV))[ind]
weight = np.concatenate((weightS,weightV))[ind]

rho_clean,drho,ddrho = logder(rho,r,weight,3)
c_clean = logder(c,r,weight,1)

#idx1 = np.min(np.where(r == a)[0])
idx1 = np.argmin(np.abs(r - a))
i0 = np.min( np.nonzero(r > 0.99)[0] ) 

f,df,ddf = logder(1/np.sqrt(rho),r,weight,3)
pot_rho = np.sqrt(rho_clean)*(ddf+2*df/r)
pot_c = c_clean

#################################################### 
# BSpline approximation of c,rho and potential 
####################################################

r_beg = r[0]
pot_rho_beg = pot_rho[0]
pot_c_beg = pot_c[0]
r_end = r[-1]
pot_rho_end = pot_rho[-1]
pot_c_end = pot_c[-1]

r = np.append([r_beg for i in range(spline_order)],r)
pot_rho = np.append([pot_rho_beg for i in range(spline_order)],pot_rho)
pot_c = np.append([pot_c_beg for i in range(spline_order)],pot_c)

r = np.append(r,[r_end for i in range(spline_order)])
pot_rho = np.append(pot_rho,[pot_rho_end for i in range(spline_order)])
pot_c = np.append(pot_c,[pot_c_end for i in range(spline_order)])

r = r.tolist()
pot_rho = pot_rho.tolist()
pot_c = pot_c.tolist()

pot_rho_B = BSpline(spline_order,r,pot_rho)(x)
pot_c_B = BSpline(spline_order,r,pot_c)(x)

pot_1D = pot_rho_B - omega**2/pot_c_B**2

###################################################### 
# Integrate wavelength
######################################################
intorder = 20

lambda_eff = 2*pi*pot_c_B/omega

mesh_1D_inner = Make1DMesh_flex(0.0,a,1000)
mesh_1D_outer = Make1DMesh_flex(a,R_max_ODE,1000)

inner_wavelength_nr = Integrate (  1/lambda_eff, mesh_1D_inner, VOL,order=intorder)
outer_wavelength_nr = Integrate (  1/lambda_eff, mesh_1D_outer, VOL,order=intorder )
print("\n")
print("# wavelength from core to photosphere = ", inner_wavelength_nr) 
print("# wavelength from photosphere to end of VAL-C model =", outer_wavelength_nr)

print("\n 1D")
Ia = Integrate (  1/lambda_eff, mesh_1D_inner, VOL,order=intorder )
IR = Ia + Integrate (  1/lambda_eff, mesh_1D_outer, VOL,order=intorder )
print("Ia =", Ia)
print("IR =", IR)
print("IR/Ia = ", IR/Ia)

print("\n 2D")
Ia = 0.5*pi*Integrate (  x**1/lambda_eff**2, mesh_1D_inner, VOL,order=intorder )
IR = Ia + 0.5*pi*Integrate (  x**1/lambda_eff**2, mesh_1D_outer, VOL,order=intorder )
print("Ia =", Ia)
print("IR =", IR)
print("IR/Ia = ", IR/Ia)

print("\n 3D")
Ia = 4*pi*Integrate (  x**2/lambda_eff**3, mesh_1D_inner, VOL,order=intorder )
IR = Ia + 4*pi*Integrate (  x**2/lambda_eff**3, mesh_1D_outer, VOL,order=intorder )
print("Ia =", Ia)
print("IR =", IR)
print("IR/Ia = ", IR/Ia)
