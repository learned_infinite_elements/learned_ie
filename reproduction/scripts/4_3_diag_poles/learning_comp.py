import numpy as np
from math import sqrt,exp
from scipy.special import hankel1,h1vp
import ceres_dtn as opt
import mpmath as mp

k = 16 # wavenumber
a = 1.0 # coupling radius
Lmax = 72 # number of modes
alpha_decay = 2/3 # decay of weights
Nmax = 7 # maximal number of infinite element dofs
Ns = list(range(Nmax))


def hankel1(nu,k):
    return mp.hankel1(nu,k)

def h1vp(nu,k):
    return 0.5*(mp.hankel1(nu-1,k) - mp.hankel1(nu+1,k)) 

def dtn_ref(nu):
    return -k*h1vp(nu,k*a) / hankel1(nu,k*a)

lam = np.array([(l/a)**2 for l in range(Lmax)]) 
dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) 
weights = np.array([10**6*np.exp(-l*alpha_decay) for l in range(Lmax)])
final_res = np.zeros(len(lam),dtype=float)

l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)

def new_initial_guess(A_old,B_old,ansatz):
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = -100-100j
        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) 
    return A_guess,B_guess


flags = {"max_num_iterations":5000000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":True,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-6,
         "parameter_tolerance":1e-8}

results = {}
ansatzes = ["minimalIC","full"]
header_str = "l "
A_IEs = {}
B_IEs = {}

for ansatz in ansatzes:
    print("Minimizing for {0} ansatz".format(ansatz))
    header_str += ansatz 
    header_str += " "
    np.random.seed(123)
    A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    A_IE = []
    B_IE = [] 
    relative_residuals = []
    for N in Ns: 
        l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)
        A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())
        A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)
    results[ansatz] = relative_residuals
    A_IEs[ansatz] = A_IE
    B_IEs[ansatz] = B_IE

# residuals

for N in Ns:
    results_N = [np.arange(Lmax)]
    for ansatz in ansatzes:
        results_N.append(results[ansatz][N])
    fname = "ansatz-residuals-"+"N{0}.dat".format(N)
    np.savetxt(fname =fname,
               X = np.transpose(results_N),
               header = header_str,
               comments = '')

# poles for reduced ansatz
print("Poles for learned infinite elements")
A_IE = A_IEs["minimalIC"]
B_IE = B_IEs["minimalIC"]
for N in Ns[1:]:
    print("poles for N = {0}".format(N))
    learned_poles = np.array([-A_IE[N][j,j] for j in range(1,N+1)])
    learned_poles = learned_poles[np.argsort(learned_poles.imag)]
    for pole in learned_poles:
        print("({0},{1})".format(pole.real,pole.imag))

print("Computing analytic poles")
from GRPF import Poles_and_Roots 
param = {}
# rectangular domain definition z=x+jy x\in[xb,xe], y \in [yb,ye]
param["xrange"] = [-500,500]
param["yrange"] = [1,3000]
param["h"] = 10 
param["Tol"] = 1e-9
param["visual"] = 1 
param["ItMax"] = 20
param["NodesMax"] = 500000 
result = Poles_and_Roots(lambda z:dtn_ref(mp.sqrt(z)*a),param)

analytic_poles = result["poles"]
analytic_poles = analytic_poles[np.argsort(analytic_poles.imag)]
print("\n analytic poles")
for pole in analytic_poles:
    print("({0},{1})".format(pole.real,pole.imag))

#print("function value at poles = ", [abs(complex(dtn_ref(mp.sqrt(z)*a))) for z in analytic_poles])
# roots/poles could be refined further e.g. by a Newton iteration

